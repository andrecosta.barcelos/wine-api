import { ApiCore } from "./utilities/core";
import { apiProvider } from "./utilities/provider";

const url = "products";
const plural = "products";
const single = "product";

const apiTasks = new ApiCore({
  getAll: true,
  getSingle: true,
  post: true,
  put: true,
  patch: true,
  remove: true,
  url: url,
  plural: plural,
  single: single,
});

apiTasks.getProducts = (
  page,
  limit,
  minPrice,
  maxPrice,
  category,
  region,
  year,
  minRating,
  maxRating
) => {
  let queryParams = "?page=" + page + "&limit=" + limit;
  if (minPrice && minPrice > 0) {
    queryParams += "&minprice=" + minPrice;
  }
  if (maxPrice && maxPrice > 0) {
    queryParams += "&maxprice=" + maxPrice;
  }
  if (category && category != "") {
    queryParams += "&category=" + category;
  }
  if (region && region != "") {
    queryParams += "&region=" + region;
  }
  if (year && year != "") {
    queryParams += "&year=" + year;
  }
  if (minRating && minRating != "0") {
    queryParams += "&minrating=" + minRating;
  }
  if (maxRating && maxRating != "0") {
    queryParams += "&maxrating=" + maxRating;
  }
  console.log(queryParams);

  return apiProvider.getAll(url + queryParams);
};

export default apiTasks;
