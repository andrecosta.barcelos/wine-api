import { ApiCore } from "./utilities/core";

const url = "files";
const plural = "files";
const single = "file";

const apiTasks = new ApiCore({
  getAll: true,
  getSingle: true,
  post: true,
  put: false,
  patch: true,
  delete: false,
  url: url,
  plural: plural,
  single: single,
});

apiTasks.massUpdate = () => {
  // Add custom api call logic here
};

export default apiTasks;
