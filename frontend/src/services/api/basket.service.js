import { ApiCore } from "./utilities/core";

const url = "baskets";
const plural = "baskets";
const single = "basket";

const apiTasks = new ApiCore({
  getAll: true,
  getSingle: false,
  post: false,
  put: false,
  patch: true,
  delete: false,
  url: url,
  plural: plural,
  single: single,
});

apiTasks.addItem = (productsId) => {
    return apiProvider.custom().post(`${options.url}/products/add`, { productId: productId });
};

apiTasks.deleteItem = (productId) => {
  return apiProvider.custom().post(`${options.url}/products/delete`, { productId: productId });
};

export default apiTasks;
