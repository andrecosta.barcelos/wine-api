import { ApiCore } from "./utilities/core";

const url = "regions";
const plural = "regions";
const single = "region";

const apiTasks = new ApiCore({
  getAll: true,
  getSingle: true,
  post: true,
  put: true,
  patch: true,
  remove: true,
  url: url,
  plural: plural,
  single: single,
});

apiTasks.massUpdate = () => {
  // Add custom api call logic here
};

export default apiTasks;
