import React, { useState } from "react";

const initialStore = {
    items: [],
    total: 0,
    quantity: 0,
}

export const AppContext = React.createContext(initialStore);

const Store = (props) => {

    const [basket, setBasket] = useState(initialStore)


    // Add item in basket
    function addToBasket(product) {
        
        let index = basket.items.findIndex(item => item.id === product.id);

        if (index === -1) {
              product.quantity = 1;
              basket.items.push(product) 
            } else { 
              basket.items[index].quantity++;
        }

        setBasket({ ...basket, items: basket.items, total: basket.total + product.price, quantity: basket.quantity + 1 });
    
    };

    // Remove item in basket
    function removeFromBasket(productId) {

        let index = basket.items.findIndex(item => item.id === productId);

        if (index !== -1) {

        let price = basket.items[index].price;
        
        basket.items[index].quantity === 1 ? basket.items.splice(index, 1) : basket.items[index].quantity--;

        setBasket({ ...basket, items: basket.items, total: basket.total - price, quantity: basket.quantity - 1 });
        }

    };

    return (
        <AppContext.Provider value={{
            basket: basket,
            addToBasket: product => addToBasket(product),
            removeFromBasket: productId => removeFromBasket(productId),
        }}>
            {props.children}
        </AppContext.Provider>
    )
}

export default Store;