import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import { QueryClient, QueryClientProvider } from 'react-query';

import { io } from "socket.io-client";
import { Notifications } from 'react-push-notification';
import addNotification from 'react-push-notification';

import ButtonAppBar from "./components/ButtonAppBar/ButtonAppBar";
import Login from "./components/Login/Login";
import WineMain from "./components/Main/WineMain";

import CategoryList from "./components/Category/CategoryList";
import CategoryItem from "./components/Category/CategoryItem";

import RegionList from "./components/Region/RegionList";
import Store from "./data/store";
import RegionItem from "./components/Region/RegionItem";

import ProductList from "./components/Product/ProductList";
import ProductItem from "./components/Product/ProductItem";
import ProductItemDetail from "./components/Product/ProductItemDetail";

import UserProvider, { useUser } from "./context/UserProvider";
import { routePaths } from "./shared/routePaths";

import "./App.css";
import { AppWrapper } from "./AppWrapper";
import PrivateRoute from "./routing/PrivateRoute";

const routes = [
  // Rotas públicas
  { id: "01", private: false, path: routePaths.PRODUCTS, component: WineMain },
  { id: "02", private: false, path: routePaths.LOGIN, component: Login },

  // Products paths
  {
    id: "03",
    private: false,
    path: routePaths.ADMIN + routePaths.PRODUCTS,
    component: ProductList,
  },
  {
    id: "04",
    private: false,
    path: `${routePaths.PRODUCTS}/:id`,
    component: ProductItemDetail,
  },
  {
    id: "05",
    private: false,
    path: routePaths.ADMIN + routePaths.PRODUCTS + routePaths.EDIT + "/:id",
    component: ProductItem,
  },
  {
    id: "06",
    private: false,
    path: routePaths.ADMIN + routePaths.PRODUCTS + routePaths.CREATE,
    component: ProductItem,
  },

  // Category paths
  {
    id: "07",
    private: false,
    path: routePaths.ADMIN + routePaths.CATEGORIES,
    component: CategoryList,
  },
  {
    id: "08",
    private: false,
    path: routePaths.ADMIN + routePaths.CATEGORIES + routePaths.EDIT + "/:id",
    component: CategoryItem,
  },
  {
    id: "09",
    private: false,
    path: routePaths.ADMIN + routePaths.CATEGORIES + routePaths.CREATE,
    component: CategoryItem,
  },

  // Region paths
  {
    id: "10",
    private: false,
    path: routePaths.ADMIN + routePaths.REGIONS,
    component: RegionList,
  },
  {
    id: "11",
    private: false,
    path: routePaths.ADMIN + routePaths.REGIONS + routePaths.EDIT + "/:id",
    component: RegionItem,
  },
  {
    id: "12",
    private: false,
    path: routePaths.ADMIN + routePaths.REGIONS + routePaths.CREATE,
    component: RegionItem,
  },
  // Rotas privadas
  // { id: '08', private: true, path: '/perfil', component: Profile },
];

const queryClient = new QueryClient()

console.log({ queryClient })

const App = () => {

  const socket = io('http://localhost:3000');

  /*   socket.on("connect", () => {
      console.log("Conectado");
    }); */

  // Push Notifications via socket
  socket.on("newProduct", (data) => {
    addNotification({
      title: data.message.title,
      message: data.message.message,
      theme: data.options.theme,
      native: data.options.native,
      closeButton: data.options.closeButton,
    });
  });

  return (
    <QueryClientProvider client={queryClient}>
      <Notifications />
      <Store>
        <UserProvider>
          <AppWrapper>
            <BrowserRouter>
              <ButtonAppBar />
              <Route exact path="/">
                <Redirect to={routePaths.PRODUCTS} />
              </Route>
              {routes.map((route) =>
                route.private ? (
                  <Switch key={route.id}>
                    <PrivateRoute exact {...route} />
                  </Switch>
                ) : (
                  <Route key={route.id} exact {...route} />
                )
              )}
            </BrowserRouter>
          </AppWrapper>
        </UserProvider>
      </Store>
    </QueryClientProvider>
  );
};

export default App;
