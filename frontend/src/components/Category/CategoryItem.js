import { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import { useParams } from "react-router-dom";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import categoryService from "../../services/api/category.service";
import Button from "@material-ui/core/Button";
import { useHistory } from "react-router-dom";
import { routePaths } from "../../shared/routePaths";

const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      margin: theme.spacing(1),
      width: "25ch",
    },
  },
}));

const CategoryItem = (props) => {
  const classes = useStyles();
  const history = useHistory();
  const [category, setCategory] = useState({
    name: "",
  });

  let isEditMode = false;
  let { id } = useParams();

  if (id) {
    isEditMode = true;
  } else {
    id = 0;
  }

  useEffect(() => {
    getCategory();
  }, []);

  function getCategory() {
    console.log(id);

    if (id == 0) {
      return;
    }

    categoryService.getSingle(id).then((response) => {
      setCategory(response);
    });
  }

  const handleSubmit = (event) => {
    event.preventDefault();

    if (isEditMode) {
      categoryService.patch(category.id, category).then((response) => {
        redirectToList();
      });
    } else {
      categoryService.post(category).then((response) => {
        redirectToList();
      });
    }
  };

  const redirectToList = () => {
    history.push(routePaths.ADMIN + routePaths.CATEGORIES);
  };

  const handleChange = (event) => {
    console.log(event.target.name);
    setCategory({ ...category, [event.target.name]: event.target.value });
  };

  return (
    <>
      <Typography variant="h5" component="h2">
        Categoria ({isEditMode ? "Editar" : "Criar"})
      </Typography>
      <form
        className={classes.root}
        noValidate
        autoComplete="off"
        onSubmit={handleSubmit}
      >
        <div>
          <TextField
            id="outlined-basic"
            label="Nome"
            variant="outlined"
            name="name"
            value={category.name}
            onChange={handleChange}
          />
        </div>

        <div>
          <Button
            variant="contained"
            color="primary"
            size="small"
            onClick={handleSubmit}
            style={{ marginLeft: 16 }}
          >
            {isEditMode ? "Guardar" : "Criar"}
          </Button>
        </div>
      </form>
    </>
  );
};

CategoryItem.propTypes = {
  id: PropTypes.any,
};

export default CategoryItem;
