import { useState } from "react";
import { useQuery } from "react-query";
import { Container, makeStyles } from "@material-ui/core";
import { DataGrid } from "@material-ui/data-grid";
import Typography from "@material-ui/core/Typography";
import { NavLink } from "react-router-dom";
import Button from "@material-ui/core/Button";
import categoryService from "../../services/api/category.service";
import { routePaths } from "../../shared/routePaths";

const columns = [
  {
    field: "",
    headerName: "",
    width: 120,
    disableClickEventBubbling: true,
    renderCell: (params) => {
      return (
        <>
          <NavLink
            to={
              routePaths.ADMIN +
              routePaths.CATEGORIES +
              routePaths.EDIT +
              "/" +
              params.row.id
            }
            style={{ textDecoration: "none" }}
          >
            <Button
              variant="contained"
              color="primary"
              size="small"
              style={{ marginLeft: 16 }}
            >
              Editar
            </Button>
          </NavLink>
        </>
      );
    },
  },
  {
    field: "name",
    headerName: "Categoria",
    width: 300,
  },
];

const defaultFilters = {
  page: 1,
  limit: 10,
};

const useStyles = makeStyles(() => ({
  title: {
    marginTop: 20,
    marginBottom: 20,
  },
  table: {
    marginTop: 20,
    height: 600,
  },
}));

const CategoryList = () => {
  const [filters, setFilters] = useState(defaultFilters);
  const [select, setSelection] = useState([]);

  const classes = useStyles();

  const {
    isLoading,
    error,
    data,
    refetch: refetchCategories,
  } = useQuery("categoriesList", async () => {
    const { results } = await categoryService.getAll();
    return results;
  });

  if (isLoading) return "A carregar...";

  if (error) return "Ocorreu um erro: " + error.message;

  const handleClickDelete = async () => {
    console.log(select);
    if (select) {
      await categoryService.remove(select[0]);
      refetchCategories();
    }
  };

  const handleRowSelection = (e) => {
    // prints correct indexes of selected rows
    console.log(e.selectionModel);

    // missing the first row selected
    setSelection(e.selectionModel);
  };

  return (
    <Container>
      <Typography variant="h5" component="h2" className={classes.title}>
        Categorias
      </Typography>
      <NavLink
        to={routePaths.ADMIN + routePaths.CATEGORIES + routePaths.CREATE}
        style={{ textDecoration: "none" }}
      >
        <Button
          variant="contained"
          color="primary"
          size="small"
        >
          Criar
        </Button>
      </NavLink>
      <Button
        variant="contained"
        color="primary"
        size="small"
        onClick={handleClickDelete}
        style={{ marginLeft: 16 }}
      >
        Apagar
      </Button>
      <DataGrid
        rows={data}
        columns={columns}
        pageSize={filters.limit}
        onSelectionModelChange={handleRowSelection}
        autoHeight={true}
        className={classes.table}
      />
    </Container>
  );
};

export default CategoryList;
