import PropTypes from "prop-types";
import WineItem from "./WineItem";
import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  item: {
    margin: "10px",
  },
}));

const WineList = (props) => {
  const wines = props.wines;

  const classes = useStyles();

  return wines.map((wine) => {
    return (
      <div className={classes.item}>
        <WineItem key={wine.id} wine={wine} />
      </div>
    );
  });
};

WineList.propTypes = {
  wines: PropTypes.array,
};

export default WineList;
