import { NavLink } from "react-router-dom";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core";
import { Rating } from "@material-ui/lab";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import { routePaths } from "../../shared/routePaths";
import Paper from "@material-ui/core/Paper";
import { useContext } from "react";
import { AppContext } from "../../data/store";

const useStyles = makeStyles((theme) => ({
  itemLink: {
    textAlign: "center",
  },
  item: {
    padding: "30px",
  },
  detailLink: {
    margin: "5px",
  },
  button: {
    textDecoration: "none",
  },
  image: {
    maxWidth: "640px",
    maxHeight: "360px",
  },
}));

const WineItem = (props) => {
  const wine = props.wine;

  const classes = useStyles();
 
  const { addToBasket } = useContext(AppContext);

  return (
    <Paper elevation={3} className={classes.itemLink}>
      <div key={wine.id} className={classes.item}>
        <img
          alt={wine.image.file}
          src={wine.image.file}
          className={classes.image}
        />
        <h4>{wine.name}</h4>
        <div>
          <Rating
            name="custom-rating-filter-operator"
            placeholder="Filter value"
            readOnly
            value={Number(wine.rating)}
            precision={1}
          />
        </div>
        <Typography variant="body2" component="p">
          {wine.price.toFixed(2)} €
        </Typography>
        <div>
          <Button
            variant="contained"
            color="primary"
            className={classes.button}
            onClick={()=> addToBasket(wine)}
          >
            Adicionar ao carrinho
          </Button>
        </div>
        <div className={classes.detailLink}>
          <NavLink
            to={`${routePaths.PRODUCTS}/${wine.id}`}
            className={classes.button}
          >
            <Button variant="contained">Ver detalhe</Button>
          </NavLink>
        </div>
      </div>
    </Paper>
  );
};

WineItem.propTypes = {
  wine: PropTypes.any,
};

export default WineItem;
