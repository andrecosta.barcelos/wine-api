import { useEffect, useState } from "react";
import { useQuery } from 'react-query';
import WineList from "./WineList";
import { Container, makeStyles } from "@material-ui/core";
import Pagination from "@material-ui/lab/Pagination";

import FormLabel from "@material-ui/core/FormLabel";
import FormControl from "@material-ui/core/FormControl";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import RadioGroup from "@material-ui/core/RadioGroup";
import Radio from "@material-ui/core/Radio";
import Slider from "@material-ui/core/Slider";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import Box from "@material-ui/core/Box";

import productService from "../../services/api/product.service";
import regionService from "../../services/api/region.service";
import categoryService from "../../services/api/category.service";

const defaultFilters = {
  page: 1,
  limit: 9,
  price: [1, 2000],
  category: "",
  region: "",
  year: "",
  rating: [1, 5],
};

const useStyles = makeStyles((theme) => ({
  root: {},
  container: {
    display: "flex",
    flexWrap: "wrap",
    margin: "50px",
  },
  formControl: {
    margin: theme.spacing(2),
    width: "100%",
  },
  slider: {
    width: "100%",
  },
}));

const useYears = () => {
  let yearsArray = [];
  yearsArray.push({ id: "", name: "Todos" });
  for (let y = 2021; y > 2019; y--) {
    yearsArray.push({ id: "" + y, name: "" + y });
  }
  return yearsArray;
};

function valuePrice(value) {
  return `${value} €`;
}

function valueRating(value) {
  return `${value}`;
}

const WineMain = () => {
  const [filters, setFilters] = useState(defaultFilters);

  const classes = useStyles();

  const years = useYears();

  const { isLoading: isLoadingRegions, data: regions } = useQuery('regionsList', async () => {
    const { results } = await regionService.getAll()
    return results
  });

  const { isLoading: isLoadingCategories, data: categories } = useQuery('categoriesList', async () => {
    const { results } = await categoryService.getAll()
    return results
  });

  const { isLoading: isLoadingProducts, data: products, refetch: refetchProducts } = useQuery('productsList', async () => {
    const { results } = await productService.getProducts(
      filters.page,
      filters.limit,
      filters.price[0],
      filters.price[1],
      filters.category,
      filters.region,
      filters.year,
      filters.rating[0],
      filters.rating[1]
    )
    return results
  });

  function handleChangePage(event, newValue) {
    filters.page = newValue;
    setFilters({ ...filters, page: newValue });
    refetchProducts();
  }

  function handleChangeRegion(event, newValue) {
    console.log(newValue);
    setFilters({ ...filters, region: newValue });
  }

  function handleChangeYear(event, newValue) {
    console.log(newValue);
    setFilters({ ...filters, year: newValue });
  }

  function handleChangeCategory(event, newValue) {
    console.log(newValue);
    setFilters({ ...filters, category: newValue });
  }

  function handleChangePrice(event, newValue) {
    console.log(newValue);
    setFilters({ ...filters, price: newValue });
  }

  function handleChangeRating(event, newValue) {
    console.log(newValue);
    setFilters({ ...filters, rating: newValue });
  }

  function handleClickSearch(event) {
    setFilters({ ...filters });
    refetchProducts();
  }

  return (
    <Container fixed maxWidth="lg">
      <Grid container spacing={3}>
        <Grid item xs={2} md={2} lg={2}>
          {/* Preço */}
          <FormControl component="fieldset" className={classes.formControl}>
            <FormLabel component="legend">Preço</FormLabel>
            <FormGroup>
              <Slider
                className={classes.slider}
                value={filters.price}
                min={1}
                max={2000}
                onChange={handleChangePrice}
                valueLabelDisplay="auto"
                aria-labelledby="range-slider"
                getAriaValueText={valuePrice}
              />
            </FormGroup>
          </FormControl>

          {/* Categories */}
          {isLoadingCategories ? "A carregar..." : (
            <FormControl component="fieldset" className={classes.formControl}>
              <FormLabel component="legend">Categoria</FormLabel>
              <FormGroup>
                <RadioGroup
                  aria-label="category"
                  name="category"
                  value={filters.category}
                  onChange={handleChangeCategory}
                >
                  <FormControlLabel
                    value={""}
                    control={<Radio color="primary" />}
                    label="Todos"
                  />
                  {categories.map((c) => (
                    <FormControlLabel
                      value={c.name}
                      control={<Radio color="primary" />}
                      label={c.name}
                    />
                  ))}
                </RadioGroup>
              </FormGroup>
            </FormControl>
          )}

          {/* Regions */}
          {isLoadingRegions ? "A carregar..." : (
            <FormControl component="fieldset" className={classes.formControl}>
              <FormLabel component="legend">Região</FormLabel>
              <FormGroup>
                <RadioGroup
                  aria-label="region"
                  name="region"
                  value={filters.region}
                  onChange={handleChangeRegion}
                >
                  <FormControlLabel
                    value={""}
                    control={<Radio color="primary" />}
                    label="Todos"
                  />
                  {regions.map((r) => (
                    <FormControlLabel
                      value={r.name}
                      control={<Radio color="primary" />}
                      label={r.name}
                    />
                  ))}
                </RadioGroup>
              </FormGroup>
            </FormControl>
          )}

          {/* Ano */}
          <FormControl component="fieldset" className={classes.formControl}>
            <FormLabel component="legend">Ano</FormLabel>
            <FormGroup>
              <RadioGroup
                aria-label="year"
                name="year"
                value={filters.year}
                onChange={handleChangeYear}
              >
                {years.map((y) => (
                  <FormControlLabel
                    value={y.id}
                    control={<Radio color="primary" />}
                    label={y.name}
                  />
                ))}
              </RadioGroup>
            </FormGroup>
          </FormControl>

          {/* Rating */}
          <FormControl component="fieldset" className={classes.formControl}>
            <FormLabel component="legend">Rating</FormLabel>
            <FormGroup>
              <Slider
                className={classes.slider}
                value={filters.rating}
                min={1}
                max={5}
                onChange={handleChangeRating}
                valueLabelDisplay="auto"
                aria-labelledby="range-slider"
                getAriaValueText={valueRating}
              />
            </FormGroup>
          </FormControl>
          <FormControl component="fieldset" className={classes.formControl}>
            <FormGroup>
              <Button
                variant="contained"
                color="primary"
                className={classes.button}
                onClick={handleClickSearch}
              >
                Pesquisar
              </Button>
            </FormGroup>
          </FormControl>
        </Grid>
        {isLoadingProducts ? "A carregar..." : (
          <Grid item xs={10}>
            <div className={classes.container}>
              <WineList wines={products} />
            </div>
            <Box display="flex" justifyContent="center">
              <Pagination
                count={10}
                page={filters.page}
                size="large"
                onChange={handleChangePage}
              />
            </Box>
          </Grid>
        )}
      </Grid>
    </Container>
  );
};

export default WineMain;
