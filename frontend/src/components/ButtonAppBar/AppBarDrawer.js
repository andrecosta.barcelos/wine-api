import { useEffect, useState, Fragment } from "react";
import { useHistory, Link } from "react-router-dom";
import { makeStyles } from "@material-ui/core";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Button from "@material-ui/core/Button";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import MenuIcon from "@material-ui/icons/Menu";
import MapIcon from "@material-ui/icons/Map";
import HomeIcon from "@material-ui/icons/Home";
import CollectionsBookmarkIcon from "@material-ui/icons/CollectionsBookmark";
import StoreIcon from "@material-ui/icons/Store";
import { routePaths } from "../../shared/routePaths";

import { useUser } from "../../context/UserProvider";

const useStyles = makeStyles({
  list: {
    width: 400,
  },

  menuItens: {
    "font-family": "Arial, Helvetica, sans-serif",
    width: "350px",
    padding: "30px",
  },
});

const Menu = () => {
  const history = useHistory();

  // Use css styles
  const classes = useStyles();

  const { user, isAuthenticated, logout } = useUser();

  // Menu position
  const anchor = "left";

  // Set state for Menu
  const [open, setOpen] = useState(false);

  // Action to open and close Menu
  const toggleMenu = (open) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    setOpen(open);
  };

  const itemsList = () => (
    <div
      className={classes.list}
      role="presentation"
      onKeyDown={toggleMenu(false)}
    >
      <div className={classes.menuItens}>
        <List component="nav" aria-label="main mailbox folders">
          <ListItem
            button
            onClick={() => {
              history.push(routePaths.PRODUCTS);
              setOpen(false);
            }}
          >
            <ListItemIcon>
              <HomeIcon />
            </ListItemIcon>
            <ListItemText primary="Principal" />
          </ListItem>

          {isAuthenticated && user.role == "admin" ? (
            <>
              <ListItem
                button
                onClick={() => {
                  history.push(routePaths.ADMIN + routePaths.REGIONS);
                  setOpen(false);
                }}
              >
                <ListItemIcon>
                  <MapIcon />
                </ListItemIcon>
                <ListItemText primary="Regiões" />
              </ListItem>

              <ListItem
                button
                onClick={() => {
                  history.push(routePaths.ADMIN + routePaths.CATEGORIES);
                  setOpen(false);
                }}
              >
                <ListItemIcon>
                  <CollectionsBookmarkIcon />
                </ListItemIcon>
                <ListItemText primary="Categorias" />
              </ListItem>

              <ListItem
                button
                onClick={() => {
                  history.push(routePaths.ADMIN + routePaths.PRODUCTS);
                  setOpen(false);
                }}
              >
                <ListItemIcon>
                  <StoreIcon />
                </ListItemIcon>
                <ListItemText primary="Produtos" />
              </ListItem>
            </>
          ) : (
            ""
          )}
        </List>
      </div>
    </div>
  );

  return (
    <div>
      <Fragment>
        <Button onClick={toggleMenu(true)}>
          <MenuIcon />
        </Button>

        <Drawer anchor={anchor} open={open} onClose={toggleMenu(false)}>
          {itemsList()}
        </Drawer>
      </Fragment>
    </div>
  );
};

export default Menu;
