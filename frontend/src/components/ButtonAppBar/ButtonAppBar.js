import React from "react";
import { useHistory, Link } from 'react-router-dom';
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";

import { useUser } from "../../context/UserProvider";
import AppBarDrawer from "./AppBarDrawer";
import Basket from "../Basket/Basket";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
}));

const ButtonAppBar = () => {
  const classes = useStyles();
  const history = useHistory();

  const { user, isAuthenticated, logout } = useUser();

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <AppBarDrawer></AppBarDrawer>
          <Typography variant="h6" className={classes.title}>
            Bacchus
          </Typography>
          <Basket></Basket>
          {isAuthenticated ? (
            <>
              {`Olá ${user.name}`}
              <Button color="inherit" onClick={logout}>Logout</Button>
            </>
          ) : (
            <Button color="inherit" onClick={() => { history.push('/login') }}>Login</Button>
          )}
        </Toolbar>
      </AppBar>
    </div>
  );
};

export default ButtonAppBar;
