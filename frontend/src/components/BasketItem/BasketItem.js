import Button from '@material-ui/core/Button';
import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles({
    wrapper: {
        display: "flex",
        "justify-content": "space-between",
        "font-family": "Arial, Helvetica, sans-serif",
        "border-bottom": "1px solid lightblue",
        "padding-bottom": "20px",
        "padding-top": "20px",
    },

    div: {
      flex: 1,
    },

    information: {
        display: "flex",
        "justify-content": "space-between",
    },

    buttons: {
        display: "flex",
        "justify-content": "space-between",
    },

    buttonsArea: {
         width: "100%",
    },

    wineImage: {
      "max-width": "80px",
      "object-fit": "cover",
      "margin-left": "40px",
    },

  });

const CartItem = ({ item, addToBasket, removeFromBasket }) => {

  const classes = useStyles();

  return (
    <div className={classes.wrapper}>
        <div className={classes.buttonsArea}>
        <h3>{item.name}</h3>
            <div className={classes.information}>
                <p>Preço: {item.price.toFixed(2)}€</p>
                <p>Total: {(item.quantity * item.price).toFixed(2)}€</p>
            </div>
            <div className={classes.buttons}>
                <Button
                size='small'
                disableElevation
                variant='contained'
                onClick={() => removeFromBasket(item.id)}
                >
                -
                </Button>
                <p>{item.quantity}</p>
                <Button
                size='small'
                disableElevation
                variant='contained'
                onClick={() => addToBasket(item)}
                >
                +
                </Button>
            </div>
        </div>
        <img className={classes.wineImage} src={item.image.file} alt={item.name} />
    </div>
  );
};

export default CartItem;