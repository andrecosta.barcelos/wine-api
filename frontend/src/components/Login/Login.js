import { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { makeStyles, Container, Typography, Button, Snackbar } from '@material-ui/core';
import Box from '@material-ui/core/Box';

import InputField from '../forms/InputField'

import isEmpty from '../../utils/isEmpty';
import checkEmptyObject from '../../utils/checkEmptyObject';

import { loginUser } from '../../actions/authActions';
import { useUser } from '../../context/UserProvider';

const useStyles = makeStyles(theme => ({
    container: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        height: 'calc(100vh - 64px)'
    },
    formContainer: {
        backgroundColor: '#F6F6F6',
    },
    box: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column',
        borderRadius: '10px',
        [theme.breakpoints.down('xl')]: {
            paddingTop: '60px',
            paddingBottom: '60px',
            paddingLeft: '30px',
            paddingRight: '30px',
        },
        [theme.breakpoints.down('xs')]: {
            paddingTop: '30px',
            paddingBottom: '20px',
            paddingLeft: '10px',
            paddingRight: '10px',
        },
    },
    input: {
        marginBottom: '16px'
    },
    loginTitle: {
        marginBottom: '30px'
    }
}));

export default function Login() {
    const history = useHistory();
    const userContext = useUser();

    const [formData, setFormData] = useState({
        email: "",
        password: "",
    });
    const [errors, setErrors] = useState({
        email: "",
        password: "",
    });
    const [snackbar, setSnackbar] = useState({ open: false, message: '' });
    const [loading, setLoading] = useState(false);

    const classes = useStyles();

    const handleChange = (event) => {
        setFormData({ ...formData, [event.target.name]: event.target.value });
    };

    if (userContext.isAuthenticated) {
        history.push('/')
    }

    const submitLogin = async e => {
        e.preventDefault();
        const { email, password } = formData;
        const localErrors = {};
        setLoading(true);

        if (isEmpty(email)) {
            localErrors.email = 'Insira o email para iniciar sessão.';
        }

        if (isEmpty(password)) {
            localErrors.password = 'Insira a sua password para iniciar sessão.';
        }

        if (checkEmptyObject(localErrors).length > 0) {
            setLoading(false);
            return setErrors(localErrors);
        }

        const infoData = { email, password };

        try {
            const result = await loginUser(infoData);
            setLoading(false);

            if (result.success) {
                // Redirect to homepage
                setSnackbar({ open: true, message: `Bem vindo` })

                const { name, email, role } = result.user;

                userContext.login(name, email, role)

                history.push('/')
            } else {
                // Error message
                setSnackbar({ open: true, message: result.message })
            }
        } catch (err) {
            setLoading(false);
            console.log({ err })
            setSnackbar({ open: true, message: 'Ocorreu um erro.' })
        }
    };

    return (
        <Container className={classes.container}>
            <Container fixed maxWidth="sm" className={classes.formContainer}>
                <Box
                    className={classes.box}
                    borderRadius="50%">

                    <Typography variant="h4" align="center" className={classes.loginTitle}>
                        Login
                    </Typography>

                    <InputField
                        key="email"
                        onChange={handleChange}
                        label='E-mail'
                        type='email'
                        name='email'
                        margin='none'
                        variant='standard'
                        value={formData.email}
                        error={errors['email']}
                        helperText={errors.email}
                        onKeyPress={
                            e => {
                                if (e.key === 'Enter') {
                                    submitLogin(e);
                                }
                            }
                        }
                    />
                    <InputField
                        key="password"
                        onChange={handleChange}
                        label='Palavra-passe'
                        type='password'
                        name='password'
                        margin='none'
                        variant='standard'
                        value={formData.password}
                        error={errors['password']}
                        helperText={errors.password}
                        onKeyPress={
                            e => {
                                if (e.key === 'Enter') {
                                    submitLogin(e);
                                }
                            }
                        }
                    />
                    <Button
                        // fullWidth
                        variant="contained"
                        color="primary"
                        onClick={submitLogin}
                        className={classes.buttonLogin}
                        disabled={loading}
                    >
                        Login
                    </Button>
                </Box>

                <Snackbar
                    open={snackbar.open}
                    autoHideDuration={2500}
                    message={snackbar.message}
                    onClose={() => setSnackbar({ ...snackbar, open: false })}
                    className={classes.snackbar}
                />
            </Container>
        </Container>
    )
}
