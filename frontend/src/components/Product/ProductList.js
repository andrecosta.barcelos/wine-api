import { useState } from "react";
import { useQuery } from "react-query";
import { Container, makeStyles } from "@material-ui/core";
import { DataGrid } from "@material-ui/data-grid";
import Typography from "@material-ui/core/Typography";
import productService from "../../services/api/product.service";
import { NavLink } from "react-router-dom";
import Button from "@material-ui/core/Button";
import { routePaths } from "../../shared/routePaths";
import { useHistory, useParams } from "react-router-dom";

const columns = [
  {
    field: "",
    headerName: "",
    width: 120,
    disableClickEventBubbling: true,
    renderCell: (params) => {
      return (
        <>
          <NavLink
            to={
              routePaths.ADMIN +
              routePaths.PRODUCTS +
              routePaths.EDIT +
              "/" +
              params.row.id
            }
            style={{ textDecoration: "none" }}
          >
            <Button
              variant="contained"
              color="primary"
              size="small"
              style={{ marginLeft: 16 }}
            >
              Editar
            </Button>
          </NavLink>
        </>
      );
    },
  },
  { field: "name", headerName: "Nome", width: 250 },
  {
    field: "price",
    headerName: "Preço",
    width: 120,
    renderCell: (params) => {
      return <>{params.row.price.toFixed(2)} €</>;
    },
  },
  { field: "year", headerName: "Ano", width: 120 },
  {
    field: "region.name",
    headerName: "Região",
    width: 200,
    renderCell: (params) => {
      return <>{params.row.region.name}</>;
    },
  },
  {
    field: "category.name",
    headerName: "Categoria",
    width: 200,
    renderCell: (params) => {
      return <>{params.row.category.name}</>;
    },
  },
];

const defaultFilters = {
  page: 1,
  limit: 10,
};

const useStyles = makeStyles(() => ({
  title: {
    marginTop: 20,
    marginBottom: 20,
  },
  table: {
    marginTop: 20,
    height: 600,
  },
}));

const ProductList = () => {
  const [filters, setFilters] = useState(defaultFilters);
  const [select, setSelection] = useState([]);
  const [page, setPage] = useState(0);

  const classes = useStyles();

  const {
    isLoading,
    error,
    data,
    refetch: refetchProducts,
  } = useQuery("productsList", async () => {
    const { results } = await productService.getProducts(page, 9);
    return results;
  });

  if (isLoading) return "A carregar...";

  if (error) return "Ocorreu um erro: " + error.message;

  const handleClickDelete = async () => {
    if (select) {
      await productService.remove(select[0]);
      refetchProducts();
    }
  };

  const handleRowSelection = (e) => {
    // prints correct indexes of selected rows
    // console.log(e.selectionModel);

    // missing the first row selected
    setSelection(e.selectionModel);
  };

  const handlePageChange = (params) => {
    const page = params.page + 1
    setPage(page);
    refetchProducts();
  };

  return (
    <Container>
      <Typography variant="h5" component="h2" className={classes.title}>
        Produtos
      </Typography>
      <NavLink
        to={routePaths.ADMIN + routePaths.PRODUCTS + routePaths.CREATE}
        style={{ textDecoration: "none", marginBottom: 20 }}
      >
        <Button variant="contained" color="primary" size="small">
          Criar
        </Button>
      </NavLink>
      <Button
        variant="contained"
        color="primary"
        size="small"
        onClick={handleClickDelete}
        style={{ marginLeft: 16 }}
      >
        Apagar
      </Button>

      <DataGrid
        rows={data}
        columns={columns}
        disableMultipleSelection={true}
        pageSize={filters.limit}
        onSelectionModelChange={handleRowSelection}
        autoHeight={true}
        className={classes.table}
        rowCount={100}
        paginationMode="server"
        onPageChange={handlePageChange}
      />
    </Container>
  );
};

export default ProductList;
