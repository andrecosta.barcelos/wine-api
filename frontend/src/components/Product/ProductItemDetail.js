import { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core";
import { useParams } from "react-router-dom";
import Grid from "@material-ui/core/Grid";
import { Rating } from "@material-ui/lab";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import productService from "../../services/api/product.service";
import { useContext } from "react";
import { AppContext } from "../../data/store";

const useStyles = makeStyles({
  main: {
    align: "center",
    justify: "center",
    margin: "50px",
  },
  itemLink: {
    textAlign: "center",
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
  details: {
    float: "center",
  },
  image: {
    maxWidth: "640px",
    maxHeight: "360px",
  },
});

const ProductItemDetail = (props) => {
  const classes = useStyles();
  const [product, setProduct] = useState({
    image: { file: "" },
    region: { name: "" },
    category: { name: "" },
  });

  const { addToBasket } = useContext(AppContext);

  let isEditMode = false;
  let { id } = useParams();

  if (id) {
    isEditMode = true;
  } else {
    id = 0;
  }

  useEffect(() => {
    getProduct();
  }, []);

  function getProduct() {
    productService.getSingle(id).then((response) => {
      console.log(response);
      setProduct(response);
    });
  }

  return (
    <>
      <Grid container spacing={3} className={classes.main}>
        <Grid item xs={4} md={4} lg={4}>
          <Paper elevation={3} className={classes.itemLink}>
            <img
              alt={product.image.file}
              src={product.image.file}
              className={classes.image}
            />
          </Paper>
        </Grid>
        <Grid item xs={8} md={8} lg={8} className={classes.details}>
          <Typography variant="h5" component="h2">
            {product.name}
          </Typography>
          <Rating
            name="custom-rating-filter-operator"
            placeholder="Filter value"
            readOnly
            value={Number(product.rating)}
            precision={1}
          />{" "}
          <Typography
            className={classes.title}
            color="textSecondary"
            gutterBottom
          >
            {product.description}
          </Typography>
          <p></p>
          Ano
          <Typography className={classes.pos} color="textSecondary">
            {product.year}
          </Typography>
          Preço
          <Typography variant="body2" component="p">
            {Number(product.price).toFixed(2)} €
          </Typography>
          <p></p>
          Região:
          <Typography className={classes.pos} color="textSecondary">
            {product.region.name}
          </Typography>
          Categoria:
          <Typography className={classes.pos} color="textSecondary">
            {product.category.name}
          </Typography>
          <Button
            variant="contained"
            color="primary"
            className={classes.button}
            onClick={() => addToBasket(product)}
          >
            Adicionar ao carrinho
          </Button>
        </Grid>
      </Grid>
    </>
  );
};

ProductItemDetail.propTypes = {
  id: PropTypes.any,
};

export default ProductItemDetail;
