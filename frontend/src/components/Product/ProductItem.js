import { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import { useContext } from "react";
import { AppContext } from "../../data/store";
import productService from "../../services/api/product.service";
import regionService from "../../services/api/region.service";
import categoryService from "../../services/api/category.service";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { useHistory, useParams } from "react-router-dom";
import { routePaths } from "../../shared/routePaths";

const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      margin: theme.spacing(1),
      width: "25ch",
    },
  },
}));

const ProductItem = (props) => {
  const classes = useStyles();
  const history = useHistory();
  const [categories, setCategories] = useState({});
  const [category, setCategory] = useState({ name: "" });
  const [regions, setRegions] = useState({});
  const [region, setRegion] = useState({ name: "" });
  const [image, setImage] = useState({
    file: "image-garrafa1.png",
    path: "/uploads/image-garrafa1.png",
    mimetype: "image/png",
    description: "Garrafa 1",
  });

  const [product, setProduct] = useState({
    name: "",
    description: "",
    year: 0,
    price: 0,
    rating: 0,
  });

  const { addToBasket } = useContext(AppContext);

  let isEditMode = false;
  let { id } = useParams();

  if (id) {
    isEditMode = true;
  } else {
    id = 0;
  }

  useEffect(async () => {
    await getRegions();
    await getCategories();
    await getProduct();
  }, []);

  async function getProduct() {
    if (id == 0) {
      return;
    }

    await productService.getSingle(id).then((response) => {
      setProduct(response);
      setCategory(response.category);
      setRegion(response.region);
    });
  }

  async function getRegions() {
    await regionService.getAll().then((response) => {
      setRegions(response.results);
    });
  }

  async function getCategories() {
    await categoryService.getAll().then((response) => {
      setCategories(response.results);
    });
  }

  const handleSubmit = (event) => {
    event.preventDefault();

    product.category = category;
    product.region = region;
    product.image = image;

    if (isEditMode) {
      productService.patch(product.id, product).then((response) => {
        redirectToList();
      });
    } else {
      productService.post(product).then((response) => {
        redirectToList();
      });
    }
  };

  const redirectToList = () => {
    history.push(routePaths.ADMIN + routePaths.PRODUCTS);
  };

  const handleChange = (event) => {
    setProduct({ ...product, [event.target.name]: event.target.value });
  };

  return (
    <>
      <Typography variant="h5" component="h2">
        Produto ({isEditMode ? "Editar" : "Criar"})
      </Typography>
      <form
        className={classes.root}
        noValidate
        autoComplete="off"
        onSubmit={handleSubmit}
      >
        <div>
          <TextField
            id="outlined-basic"
            label="Nome"
            variant="outlined"
            name="name"
            value={product.name}
            onChange={handleChange}
          />
        </div>

        <div>
          <TextField
            id="outlined-basic"
            label="Descrição"
            variant="outlined"
            name="description"
            value={product.description}
            onChange={handleChange}
          />
        </div>

        <div>
          <TextField
            id="outlined-basic"
            label="Ano"
            variant="outlined"
            name="year"
            type="number"
            value={product.year}
            onChange={handleChange}
          />
        </div>

        <div>
          <TextField
            id="outlined-basic"
            label="Preço"
            variant="outlined"
            name="price"
            type="number"
            value={product.price}
            onChange={handleChange}
          />
        </div>

        <div>
          <TextField
            id="outlined-basic"
            label="Rating"
            variant="outlined"
            name="rating"
            type="number"
            value={product.rating}
            onChange={handleChange}
          />
        </div>

        <div>
          <Autocomplete
            id="combo-box-demo"
            options={regions}
            value={region}
            onChange={(event, newValue) => {
              setRegion(newValue);
            }}
            getOptionLabel={(option) => option.name}
            style={{ width: 300 }}
            renderInput={(params) => (
              <TextField {...params} label="Região" variant="outlined" />
            )}
          />
        </div>

        <div>
          <Autocomplete
            id="combo-box-demo"
            options={categories}
            value={category}
            onChange={(event, newValue) => {
              setCategory(newValue);
            }}
            getOptionLabel={(option) => option.name}
            style={{ width: 300 }}
            renderInput={(params) => (
              <TextField {...params} label="Categoria" variant="outlined" />
            )}
          />
        </div>

        <Button
          variant="contained"
          color="primary"
          size="small"
          onClick={handleSubmit}
          style={{ marginLeft: 16 }}
        >
          {isEditMode ? "Guardar" : "Criar"}
        </Button>
      </form>
    </>
  );
};

export default ProductItem;
