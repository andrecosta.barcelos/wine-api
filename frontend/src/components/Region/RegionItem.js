import { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import regionService from "../../services/api/region.service";
import Button from "@material-ui/core/Button";
import { routePaths } from "../../shared/routePaths";
import { useHistory, useParams } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      margin: theme.spacing(1),
      width: "25ch",
    },
  },
}));

const RegionItem = (props) => {
  const classes = useStyles();
  const history = useHistory();
  const [region, setRegion] = useState({
    name: "",
    location: {
      coordinates: [0.0, 0.0],
    },
  });
  const [coordinates, setCoordinates] = useState([12.3, 14.3]);

  let isEditMode = false;
  let { id } = useParams();

  if (id) {
    isEditMode = true;
  } else {
    id = 0;
  }

  useEffect(() => {
    getRegion();
  }, []);

  function getRegion() {
    console.log(id);

    if (id == 0) {
      return;
    }

    regionService.getSingle(id).then((response) => {
      setRegion(response);
    });
  }

  const handleSubmit = (event) => {
    event.preventDefault();

    region.location.coordinates = coordinates;

    console.log(region);

    if (isEditMode) {
      regionService.patch(region.id, region).then((response) => {
        redirectToList();
      });
    } else {
      regionService.post(region).then((response) => {
        redirectToList();
      });
    }
  };

  const redirectToList = () => {
    history.push(routePaths.ADMIN + routePaths.REGIONS);
  };

  const handleChange = (event) => {
    console.log(event.target.name);
    setRegion({ ...region, [event.target.name]: event.target.value });
  };

  const handleChangeLocationLatitude = (event) => {
    console.log(event.target.name + "  " + event.target.value);
    setCoordinates({ ...coordinates, [0]: event.target.value });
  };

  const handleChangeLocationLongitude = (event) => {
    console.log(event.target.name + "  " + event.target.value);
    setCoordinates({ ...coordinates, [1]: event.target.value });
  };

  return (
    <>
      <Typography variant="h5" component="h2">
        Região ({isEditMode ? "Editar" : "Criar"})
      </Typography>
      <form
        className={classes.root}
        noValidate
        autoComplete="off"
        onSubmit={handleSubmit}
      >
        <div>
          <TextField
            id="outlined-basic"
            label="Nome"
            variant="outlined"
            name="name"
            value={region.name}
            onChange={handleChange}
          />
        </div>

        <div>
          <TextField
            id="outlined-basic"
            label="Latitude"
            variant="outlined"
            name="coordinates[0]"
            type="number"
            value={coordinates[0]}
            onChange={handleChangeLocationLatitude}
          />
        </div>

        <div>
          <TextField
            id="outlined-basic"
            label="Longitude"
            variant="outlined"
            name="coordinates[1]"
            type="number"
            value={coordinates[1]}
            onChange={handleChangeLocationLongitude}
          />
        </div>

        <div>
          <Button
            variant="contained"
            color="primary"
            size="small"
            onClick={handleSubmit}
            style={{ marginLeft: 16 }}
          >
            {isEditMode ? "Guardar" : "Criar"}
          </Button>
        </div>
      </form>
    </>
  );
};

export default RegionItem;
