import { useEffect, useState, Fragment } from "react";
import { makeStyles } from "@material-ui/core";

import Drawer from '@material-ui/core/Drawer';
import Button from "@material-ui/core/Button";
import Badge from "@material-ui/core/Badge";
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';

import BasketItem from "../BasketItem/BasketItem";
import { useContext } from "react";
import { AppContext } from "../../data/store";


import basketService from "../../services/api/basket.service";

const useStyles = makeStyles({
  list: {
    width: 400,
  },

  basket: {
    "font-family": "Arial, Helvetica, sans-serif",
    width: "350px",
    padding: "30px",
  }

});

const Basket = () => {


  // Use css styles
  const classes = useStyles();

  // Basket position
  const anchor = 'right';

  const { addToBasket, removeFromBasket, basket } = useContext(AppContext);

  // Set state for Basket
  const [basketDrawler, setBasketDrawler] = useState(false);

  // Action to open and close basket
  const toggleBasket = (open) => (event) => {
    if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }
    setBasketDrawler(open);
  };

  // Basket list items
  const itemsList = () => (
    <div
      className={classes.list}
      role="presentation"
      /* onClick={toggleBasket(false)} */
      onKeyDown={toggleBasket(false)}
    >
      <aside className={classes.basket}>
          <h2>Carrinho</h2>
          {basket.items.length === 0 ? <p>Sem produtos no carrinho.</p> : null}
          {basket.items.map(item => (
            <BasketItem
              key={item.id}
              item={item}
              addToBasket={addToBasket}
              removeFromBasket={removeFromBasket}
            />
          ))}
          <h2>Total: {basket.total.toFixed(2)}€</h2>
      </aside>
    </div>
  );

  return (
    <div>
        <Fragment>

          <Button onClick={toggleBasket(true)}>
            <Badge badgeContent={basket.quantity} color="error">
               <ShoppingCartIcon />
            </Badge>
          </Button>

          <Drawer anchor={anchor} open={ basketDrawler } onClose={toggleBasket(false)}>
            { itemsList() }
          </Drawer>

        </Fragment>
    </div>
  );

};

export default Basket;
