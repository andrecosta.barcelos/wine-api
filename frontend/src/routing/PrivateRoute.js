import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { useUser } from '../context/UserProvider';

const PrivateRoute = ({ component: Component, ...rest }) => {
    const redirection = '/';

    const { user, isAuthenticated } = useUser();

    return <Route {...rest} render={props => (isAuthenticated && user.role == 'admin' ? <Component {...props} /> : <Redirect to={redirection} />)} />;
};

PrivateRoute.propTypes = {
    component: PropTypes.oneOfType([PropTypes.func, PropTypes.object]).isRequired,
};

export default PrivateRoute;
