const LOGIN = "/login";
const ADMIN = "/admin";
const PRODUCTS = "/products";
const REGIONS = "/regions";
const CATEGORIES = "/categories";

const EDIT = "/edit";
const CREATE = "/create";

export const routePaths = {
  LOGIN,
  ADMIN,
  PRODUCTS,
  REGIONS,
  CATEGORIES,
  EDIT,
  CREATE,
};
