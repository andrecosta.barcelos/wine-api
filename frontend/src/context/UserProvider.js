import { useState, useContext, createContext } from "react";

const UserContext = createContext();
export const useUser = () => useContext(UserContext);

const initialUserData = {
    name: null,
    email: null,
    role: null
}

const UserProvider = ({ children }) => {
    const [isAuthenticated, setIsAuthenticated] = useState(false);
    const [user, setUser] = useState(initialUserData);

    const login = (name, email, role) => {
        setUser((user) => ({
            name,
            email,
            role
        }));

        setIsAuthenticated(true);
    };

    const logout = () => {
        setUser((user) => (initialUserData));

        localStorage.removeItem('jwtToken')
        localStorage.removeItem('jwtExpires')

        localStorage.removeItem('refreshToken')
        localStorage.removeItem('refreshExpires')

        setIsAuthenticated(false);
    };

    return (
        <UserContext.Provider value={{ user, isAuthenticated, login, logout }}>
            {children}
        </UserContext.Provider>
    );
};

export default UserProvider;