const getItemFromStorage = item => {
    return JSON.parse(localStorage.getItem(item)) || [];
};

export default getItemFromStorage;
