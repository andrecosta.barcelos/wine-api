const pushItemToStorage = (item, info) => {
    const itemStorage = JSON.parse(localStorage.getItem(item));
    itemStorage.push(info);
    return localStorage.setItem(item, JSON.stringify(itemStorage));
};

export default pushItemToStorage;
