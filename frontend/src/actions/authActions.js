import axios from 'axios';
import setAuthToken from '../utils/setAuthToken';
import isEmpty from '../utils/isEmpty';
// const config = require('../config/config');

export const loginUser = loginData => {
    return axios
        .post(`http://localhost:3000/v1/auth/login`, loginData)
        .then(res => {
            // Save token to sessionStorage
            const { token, expires } = res.data.tokens.access;
            const { name, email, role } = res.data.user;

            const refreshToken = res.data.tokens?.refresh.token;
            const refreshExpires = res.data.tokens?.refresh.expires;

            if (!isEmpty({ token, expires })) {
                localStorage.setItem('jwtToken', token);
                localStorage.setItem('jwtExpires', expires);
            }

            if (!isEmpty({ refreshToken, refreshExpires })) {
                localStorage.setItem('refreshToken', refreshToken);
                localStorage.setItem('refreshExpires', refreshExpires);
            }

            // Set token to auth header
            setAuthToken(token);

            return {
                success: true,
                user: { name, email, role }
            };

        })
        .catch(err => {
            console.log({ err })
            return {
                success: false,
                message: err.response?.data?.message ?? 'Ocorreu um erro'
            };
        });
};

export const getUserInfo = () => {
    return axios
        .get(`http://localhost:3000/v1/auth/user`)
        .then(res => {
            const { name, email, role } = res.data;

            return {
                success: true,
                user: { name, email, role }
            };
        })
        .catch(err => {
            return {
                success: false,
                message: err.response?.data?.message ?? 'Ocorreu um erro'
            };
        });
};

export const refreshToken = () => {
    const token = localStorage.getItem('refreshToken');

    return axios
        .post(`http://localhost:3000/v1/auth/refresh-tokens`, {
            refreshToken: token
        })
        .then(res => {
            // Save token to sessionStorage
            const { token, expires } = res.data.access;

            const refreshToken = res.data.refresh?.token;
            const refreshExpires = res.data.refresh?.expires;

            if (!isEmpty({ token, expires })) {
                localStorage.setItem('jwtToken', token);
                localStorage.setItem('jwtExpires', expires);
            }

            if (!isEmpty({ refreshToken, refreshExpires })) {
                localStorage.setItem('refreshToken', refreshToken);
                localStorage.setItem('refreshExpires', refreshExpires);
            }

            // Set token to auth header
            setAuthToken(token);

            return {
                success: true
            };

        })
        .catch(err => {
            console.log({ err })
            return {
                success: false,
                message: err.response?.data?.message ?? 'Ocorreu um erro'
            };
        });
}