import { useEffect } from "react";
import { useUser } from "./context/UserProvider";
import setAuthToken from "./utils/setAuthToken";

import { getUserInfo, refreshToken } from './actions/authActions';

export const AppWrapper = ({ children }) => {
  const userContext = useUser();

  useEffect(async () => {

    if (localStorage.getItem('jwtToken')) {
      setAuthToken(localStorage.getItem('jwtToken'))

      const expires = localStorage.getItem('jwtExpires');

      if(expires) {
        const currentTime = Date.now();
        const expiresTime = Date.parse(expires);

        if (expiresTime < currentTime) {
          if (localStorage.getItem('jwtToken')) {
            try {
              const result = await refreshToken();
  
              if(result.success) {
                const userInfo = await getUserInfo();
    
                if (userInfo.user) {
                  const { name, email, role } = userInfo.user
    
                  userContext.login(name, email, role)
                }
              } else {
                userContext.logout();
              }
  
            } catch (err) {
              userContext.logout();
              console.log({ err })
            }
          } else {
            userContext.logout();
            return;
          }
        }
      }

      try {
        const result = await getUserInfo();

        if (result.success && result.user) {
          const { name, email, role } = result.user

          userContext.login(name, email, role)
        } else {
          userContext.logout();
        }

      } catch (err) {
        userContext.logout();
        console.log({ err })
      }
    }
  }, [])


  return <>{children}</>
}