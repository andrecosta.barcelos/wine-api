const { generateTemplateFiles } = require('generate-template-files');

generateTemplateFiles([
  {
    option: 'model',
    defaultCase: '(camelCase)',
    entry: {
      folderPath: './tools/templates/__model__.model.js',
    },
    stringReplacers: [{ question: 'Insert model name', slot: '__model__' }],
    output: {
      path: './src/models/__model__.model.js',
      pathAndFileNameDefaultCase: '(camelCase)',
      overwrite: true,
    },
    onComplete: (results) => {
      console.log(`results`, results);
    },
  },

  {
    option: 'controller',
    defaultCase: '(camelCase)',
    entry: {
      folderPath: './tools/templates/__name__.controller.js',
    },
    stringReplacers: [{ question: 'Insert controller name', slot: '__name__' }],
    output: {
      path: './src/controllers/__name__.controller.js',
      pathAndFileNameDefaultCase: '(camelCase)',
    },
    onComplete: (results) => {
      console.log(`results`, results);
    },
  },


  {
    option: 'service',
    defaultCase: '(camelCase)',
    entry: {
      folderPath: './tools/templates/__name__.service.js',
    },
    stringReplacers: [{ question: 'Insert service name', slot: '__name__' }],
    output: {
      path: './src/services/__name__.service.js',
      pathAndFileNameDefaultCase: '(camelCase)',
    },
    onComplete: (results) => {
      console.log(`results`, results);
    },
  },
]);

/* (noCase)        // Lives down BY the River
(camelCase)     // livesDownByTheRiver
(constantCase)  // LIVES_DOWN_BY_THE_RIVER
(dotCase)       // lives.down.by.the.river
(kebabCase)     // lives-down-by-the-river
(lowerCase)     // livesdownbytheriver
(pascalCase)    // LivesDownByTheRiver
(pathCase)      // lives/down/by/the/river
(sentenceCase)  // Lives down by the river
(snakeCase)     // lives_down_by_the_river
(titleCase)     // Lives Down By The River
 */
