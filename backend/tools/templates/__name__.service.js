const httpStatus = require('http-status');
const { __name__(pascalCase) } = require('../models');
const ApiError = require('../utils/ApiError');

/**
 * Create a __name__
 * @param {Object} __name__Body
 * @returns {Promise<__name__(pascalCase)>}
 */
const create = async (__name__Body) => {
  const __name__ = await __name__(pascalCase).create(__name__Body);
  return __name__;
};

/**
 * Query for __name__s
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const getAll = async (filter, options) => {
  const __name__s = await __name__(pascalCase).paginate(filter, options);
  return __name__s;
};

/**
 * Get __name__ by id
 * @param {ObjectId} id
 * @returns {Promise<__name__(pascalCase)>}
 */
const getById = async (id) => {
  return __name__(pascalCase).findById(id);
};

/**
 * Update __name__(pascalCase) by id
 * @param {ObjectId} __name__Id
 * @param {Object} updateBody
 * @returns {Promise<__name__(pascalCase)>}
 */
const updateById = async (__name__Id, updateBody) => {
  const __name__ = await getById(__name__Id);
  if (!__name__) {
    throw new ApiError(httpStatus.NOT_FOUND, '__name__(pascalCase) not found');
  }
  Object.assign(__name__, updateBody);
  await __name__.save();
  return __name__;
};

/**
 * Delete __name__ by id
 * @param {ObjectId} __name__Id
 * @returns {Promise<__name__(pascalCase)>}
 */
const deleteById = async (__name__Id) => {
  const __name__ = await getById(__name__Id);
  if (!__name__) {
    throw new ApiError(httpStatus.NOT_FOUND, '__name__ not found');
  }
  await __name__.remove();
  return __name__;
};

module.exports = {
  create,
  getAll,
  getById,
  updateById,
  deleteById,
};
