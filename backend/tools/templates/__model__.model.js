const mongoose = require('mongoose');
const { toJSON } = require('./plugins');

const __model__Schema = mongoose.Schema(
  {},
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
__model__Schema.plugin(toJSON);

/**
 * @typedef Token
 */
const __model__(pascalCase) = mongoose.model('__model__(pascalCase)', __model__Schema);

module.exports = __model__(pascalCase);
