const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { __name__Service } = require('../services');

const create__name__(pascalCase) = catchAsync(async (req, res) => {
  const __name__ = await __name__Service.create(req.body);
  res.status(httpStatus.CREATED).send(__name__);
});

const get__name__(pascalCase)s = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const result = await __name__Service.getAll(filter, options);
  res.send(result);
});

const get__name__(pascalCase) = catchAsync(async (req, res) => {
  const __name__ = await __name__Service.getById(req.params.id);
  if (!__name__) {
    throw new ApiError(httpStatus.NOT_FOUND, '__name__(pascalCase) not found');
  }
  res.send(__name__);
});

const update__name__(pascalCase) = catchAsync(async (req, res) => {
  const __name__ = await __name__Service.updateById(req.params.id, req.body);
  res.send(__name__);
});

const delete__name__(pascalCase) = catchAsync(async (req, res) => {
  await __name__Service.deleteById(req.params.id);
  res.status(httpStatus.NO_CONTENT).send();
});

module.exports = {
  create__name__(pascalCase),
  get__name__(pascalCase)s,
  get__name__(pascalCase),
  update__name__(pascalCase),
  delete__name__(pascalCase),
};
