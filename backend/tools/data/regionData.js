const regionData = [
  {
    name: 'Douro',
    location: {
      coordinates: [12.23, 14.23],
    },
  },
  {
    name: 'Dão',
    location: {
      coordinates: [12.23, 14.23],
    },
  },
  {
    name: 'Alentejo',
    location: {
      coordinates: [12.23, 14.23],
    },
  },
];

module.exports = regionData;
