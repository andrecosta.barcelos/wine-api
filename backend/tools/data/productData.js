const regionData = require('./regionData');
const categoryData = require('./categoryData');
const fileData = require('./fileData');
var _ = require('lodash');

const productData = [
  {
    name: 'Cabeça de Burro',
    price: 12.32,
    description: 'Bom vinho tinto maduro',
    rating: 4.2,
    year: 2016,
    image: fileData[0],
    category: categoryData[0],
    region: regionData[0],
  },
  {
    name: 'Monte Velho',
    price: 7.26,
    description: 'Bom vinho tinto maduro do Alentejo. Bom com pratos de carne',
    rating: 3.2,
    year: 2018,
    image: fileData[1],
    category: categoryData[1],
    region: regionData[1],
  },
  {
    name: 'Rasgos da Telha',
    price: 9.23,
    description: 'Bom vinho verde',
    rating: 4.8,
    year: 2004,
    image: fileData[2],
    category: categoryData[2],
    region: regionData[2],
  },
];

const getRandomGenerator = (min, max) => {
  return _.random(min, max);
};

const SeedProduct = () => {
  for (let i = 3; i < 1000; i++) {
    const categoryIndex = getRandomGenerator(0, 2);
    const regionIndex = getRandomGenerator(0, 2);
    const imageIndex = getRandomGenerator(0, 2);

    productData[i] = {
      name: 'Vinho ' + i,
      price: getRandomGenerator(4.12, 678.54),
      description: 'O vinho número ' + i + ' é um grande vinho, perfeito para beber com tudo. ',
      rating: getRandomGenerator(1.0, 5.0),
      year: getRandomGenerator(2002, 2021),
      image: fileData[imageIndex],
      category: categoryData[categoryIndex],
      region: regionData[regionIndex],
    };
  }
};

module.exports = { productData, SeedProduct };
