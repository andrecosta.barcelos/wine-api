const fileData = [
  {
    file: 'image-garrafa1.png',
    path: '/uploads/image-garrafa1.png',
    mimetype: 'image/png',
    description: 'Garrafa 1',
  },
  {
    file: 'image-garrafa2.png',
    path: '/uploads/image-garrafa2.png',
    mimetype: 'image/png',
    description: 'Garrafa 2',
  },
  {
    file: 'image-garrafa3.png',
    path: '/uploads/image-garrafa3.png',
    mimetype: 'image/png',
    description: 'Garrafa 3',
  },
];

module.exports = fileData;
