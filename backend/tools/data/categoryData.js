const categoryData = [
  {
    name: 'Vinho Verde',
  },
  {
    name: 'Tinto Maduro',
  },
  {
    name: 'Tinto Verde',
  },
];

module.exports = categoryData;
