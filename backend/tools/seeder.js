var seeder = require('mongoose-seed');
var config = require('../src/config/config');

const regionData = require('./data/regionData');
const categoryData = require('./data/categoryData');
const fileData = require('./data/fileData');
const { productData, SeedProduct } = require('./data/productData');

const modelsPatch = 'src/models/';

// Connect to MongoDB via Mongoose
seeder.connect(config.mongoose.url, function () {
  SeedProduct();

  // Load Mongoose models
  seeder.loadModels([
    modelsPatch + 'category.model.js',
    modelsPatch + 'region.model.js',
    modelsPatch + 'product.model.js',
    modelsPatch + 'file.model.js',
    /*   modelsPatch + 'user.model.js',
    modelsPatch + 'basket.model.js',
    modelsPatch + 'basketProduct.model..js', */
  ]);

  //console.log('AMEN: ' + categoryData[0]);

  // Clear specified collections
  seeder.clearModels(['Category', 'Region', 'Product', 'File'], function () {
    // Callback to populate DB once collections have been cleared
    seeder.populateModels(data, function () {
      seeder.disconnect();
    });
  });
});

// Data array containing seed data - documents organized by Model
var data = [
  {
    model: 'Region',
    documents: regionData,
  },
  {
    model: 'Category',
    documents: categoryData,
  },
  {
    model: 'Product',
    documents: productData,
  },
    {
    model: 'File',
    documents: fileData,
  },
];
