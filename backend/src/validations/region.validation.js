const Joi = require('joi');
const { objectId } = require('./custom.validation');

const createRegion = {
  body: Joi.object().keys({
    name: Joi.string().required(),
    location: Joi.any(),
  }),
};

const getRegions = {
  query: Joi.object().keys({
    name: Joi.string(),
    location: Joi.any(),
  }),
};

const getRegion = {
  params: Joi.object().keys({
    id: Joi.string(),
  }),
};

const updateRegion = {
  params: Joi.object().keys({
    id: Joi.required(),
  }),
  body: Joi.object()
    .keys({
      name: Joi.string().required(),
      location: Joi.any(),
    })
    .min(1),
};

const deleteRegion = {
  params: Joi.object().keys({
    id: Joi.string().custom(objectId),
  }),
};

module.exports = {
  createRegion,
  getRegions,
  getRegion,
  updateRegion,
  deleteRegion,
};
