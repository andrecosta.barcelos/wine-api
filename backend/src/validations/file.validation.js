const Joi = require('joi');
const { objectId, imagesTypes } = require('./custom.validation');

const createFile = {
  body: Joi.object().keys({
    // file: Joi.required().custom(imagesTypes),
    description: Joi.string(),
  }),
};

const getFiles = {
  query: Joi.object().keys({
    mimetype: Joi.string(),
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer(),
  }),
};

const getFile = {
  params: Joi.object().keys({
    id: Joi.string().custom(objectId),
  }),
};

const updateFile = {
  params: Joi.object().keys({
    id: Joi.string().custom(objectId),
  }),
  body: Joi.object()
    .keys({
      description: Joi.string(),
    })
    .min(1),
};

const deleteFile = {
  params: Joi.object().keys({
    id: Joi.string().custom(objectId),
  }),
};

module.exports = {
  createFile,
  getFiles,
  getFile,
  updateFile,
  deleteFile,
};
