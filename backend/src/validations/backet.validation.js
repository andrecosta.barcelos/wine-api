const Joi = require('joi');
const { objectId } = require('./custom.validation');


const getBackets = {
  query: Joi.object().keys({
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer(),
  }),
};

const getBacket = {
  params: Joi.object().keys({
    id: Joi.string(),
  }),
};

const addProduct = {
  body: Joi.object()
    .keys({
      productId: Joi.string().required(),
    })
};

const deleteProduct = {
  body: Joi.object()
    .keys({
      productId: Joi.string().required(),
    })
};

module.exports = {
  getBackets,
  getBacket,
  addProduct,
  deleteProduct,
};
