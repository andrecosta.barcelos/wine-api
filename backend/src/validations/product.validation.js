const Joi = require('joi');
const { objectId } = require('./custom.validation');

const createProduct = {
  body: Joi.object().keys({
    name: Joi.string().required(),
    price: Joi.number(),
    description: Joi.string().required(),
    rating: Joi.number().integer(),
    year: Joi.number().integer(),
    category: Joi.any(),
    region: Joi.any(),
    image: Joi.any(),
  }),
};

const getProducts = {
  query: Joi.object().keys({
    name: Joi.string(),
    minprice: Joi.number(),
    maxprice: Joi.number(),
    minrating: Joi.number(),
    maxrating: Joi.number(),
    year: Joi.number().integer(),
    category: Joi.string(),
    region: Joi.string(),
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer(),
  }),
};

const getProduct = {
  params: Joi.object().keys({
    id: Joi.string(),
  }),
};

const updateProduct = {
  params: Joi.object().keys({
    id: Joi.required(),
  }),
  body: Joi.object()
    .keys({
      name: Joi.string().required(),
    })
    .min(1),
};

const deleteProduct = {
  params: Joi.object().keys({
    id: Joi.string().custom(objectId),
  }),
};

module.exports = {
  createProduct,
  getProducts,
  getProduct,
  updateProduct,
  deleteProduct,
};
