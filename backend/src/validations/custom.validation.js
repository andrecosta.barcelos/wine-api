const Types = require('../config/filesTypes');

const objectId = (value, helpers) => {
  if (!value.match(/^[0-9a-fA-F]{24}$/)) {
    return helpers.message('"{{#label}}" must be a valid mongo id');
  }
  return value;
};

const password = (value, helpers) => {
  if (value.length < 8) {
    return helpers.message('password must be at least 8 characters');
  }
  if (!value.match(/\d/) || !value.match(/[a-zA-Z]/)) {
    return helpers.message('password must contain at least 1 letter and 1 number');
  }
  return value;
};

const imagesTypes = (file, helpers) => {

  // GET file image types
  const imageTypes = Types.filter(type => type.type === 'IMAGE');

  // Valid image type
  const list = imageTypes.filter(imageType => imageType.mimetype === file.mimetype);

  if (list.length !== 1) {
    return helpers.message('invalid image type');
  }
  return file;
};

module.exports = {
  objectId,
  password,
  imagesTypes,
};
