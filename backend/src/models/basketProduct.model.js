const mongoose = require('mongoose');
const { toJSON, paginate } = require('./plugins');

const basketProductSchema = mongoose.Schema(
  {
    product: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'Product',
      required: true,
    },
    quantity: {
      type: Number,
      required: true,
    },
    total: {
      type: Number,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
basketProductSchema.plugin(toJSON);
basketProductSchema.plugin(paginate);

/**
 * @typedef BasketProduct
 */
const BasketProduct = mongoose.model('BasketProduct', basketProductSchema);

module.exports = BasketProduct;
