const mongoose = require('mongoose');
const { toJSON, paginate } = require('./plugins');
const Category = require('./category.model');
const Region = require('./region.model');
const File = require('./file.model');

const productSchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      trim: true,
    },
    price: {
      type: Number,
      required: true,
    },
    description: {
      type: String,
      required: true,
    },
    rating: {
      type: Number,
      required: false,
      default: 0,
    },
    year: {
      type: Number,
      required: false,
      default: 0,
    },
    category: {
      type: Category.schema,
      required: true,
    },
    region: {
      type: Region.schema,
      required: true,
    },
    image: {
      type: File.schema,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
productSchema.plugin(toJSON);
productSchema.plugin(paginate);

/**
 * @typedef Product
 */
const Product = mongoose.model('Product', productSchema);

module.exports = Product;
