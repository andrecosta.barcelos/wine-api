module.exports.Token = require('./token.model');
module.exports.User = require('./user.model');
module.exports.Category = require('./category.model');
module.exports.Region = require('./region.model');
module.exports.Product = require('./product.model');
module.exports.File = require('./file.model');
module.exports.Basket = require('./basket.model');
