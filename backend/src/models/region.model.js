const mongoose = require('mongoose');
const { toJSON, paginate } = require('./plugins');

const regionSchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    location: {
      type: {
        type: String,
        enum: ['Point'],
        default: 'Point',
      },
      coordinates: {
        type: [Number],
        default: [0, 0],
      },
    },
  },
  {
    timestamps: true,
  }
);

//regionSchema.index({ location: '2dsphere' });
// add plugin that converts mongoose to json and paginate
regionSchema.plugin(toJSON);
regionSchema.plugin(paginate);

/**
 * @typedef Region
 */
const Region = mongoose.model('Region', regionSchema);

module.exports = Region;
