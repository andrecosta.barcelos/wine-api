const { date } = require('joi');
const mongoose = require('mongoose');
const { toJSON, paginate } = require('./plugins');
const BasketProduct = require('./basketProduct.model');

const basketSchema = mongoose.Schema(
  {
    date: {
      type: String,
      required: true,
    },
    total: {
      type: Number,
      required: true,
    },
    userId: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'User',
      required: true,
    },
    products: {
      children: BasketProduct.schema,
    },
  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
basketSchema.plugin(toJSON);
basketSchema.plugin(paginate);

/**
 * @typedef Basket
 */
const Basket = mongoose.model('Basket', basketSchema);

module.exports = Basket;
