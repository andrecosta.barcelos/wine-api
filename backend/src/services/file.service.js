const httpStatus = require('http-status');
const fs = require('fs');
const File  = require('../models/file.model');
const ApiError = require('../utils/ApiError');
const logger = require('../config/logger');

// Delete file in storage
const deleteFile = (filePath) => {
  try {
    fs.unlinkSync(filePath);
  } catch (e) {
    logger.error(`Error on deleting file: ${e}`);
  }
}

/**
 * Create a file
 * @param {Object} fileBody
 * @returns {Promise<File>}
 */
const create = async (fileBody) => {
  const file = await File.create(fileBody);
  return file;
};

/**
 * Query for files
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const getAll = async (filter, options) => {
  const files = await File.paginate(filter, options);
  return files;
};

/**
 * Get file by id
 * @param {ObjectId} id
 * @returns {Promise<File>}
 */
const getById = async (id) => {
  return File.findById(id);
};

/**
 * Update File by id
 * @param {ObjectId} fileId
 * @param {Object} updateBody
 * @returns {Promise<File>}
 */
const updateById = async (fileId, updateBody) => {
  const file = await getById(fileId);
  if (!file) {
    throw new ApiError(httpStatus.NOT_FOUND, 'File not found');
  }
  Object.assign(file, updateBody);

  // Remove old file
  deleteFile(file.path);

  await file.save();

  return file;
};

/**
 * Delete file by id
 * @param {ObjectId} fileId
 * @returns {Promise<File>}
 */
const deleteById = async (fileId) => {
  const file = await getById(fileId);
  if (!file) {
    throw new ApiError(httpStatus.NOT_FOUND, 'file not found');
  }
  await file.remove();

  deleteFile(file.path);

  return file;
};

module.exports = {
  create,
  getAll,
  getById,
  updateById,
  deleteById,
};
