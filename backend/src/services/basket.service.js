const httpStatus = require('http-status');
const { Basket, Product } = require('../models');
const ApiError = require('../utils/ApiError');

/**
 * Create a basket
 * @param {Object} basketBody
 * @returns {Promise<Basket>}
 */
const create = async (basketBody) => {
  const basket = await Basket.create(basketBody);
  return basket;
};

/**
 * Query for baskets
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const getAll = async (filter, options) => {
  const baskets = await Basket.paginate(filter, options);
  return baskets;
};

/**
 * Get basket by id
 * @param {ObjectId} id
 * @returns {Promise<Basket>}
 */
const getById = async (id) => {
  return Basket.findById(id);
};

/**
 * Update Basket by id
 * @param {ObjectId} basketId
 * @param {Object} updateBody
 * @returns {Promise<Basket>}
 */
const updateById = async (basketId, updateBody) => {
  const basket = await getById(basketId);
  if (!basket) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Basket not found');
  }
  Object.assign(basket, updateBody);
  await basket.save();
  return basket;
};

/**
 * Delete basket by id
 * @param {ObjectId} basketId
 * @returns {Promise<Basket>}
 */
const deleteById = async (basketId) => {
  const basket = await getById(basketId);
  if (!basket) {
    throw new ApiError(httpStatus.NOT_FOUND, 'basket not found');
  }
  await basket.remove();
  return basket;
};

/**
 * Add Item in basket
 * @param {ObjectId} userId
 * @param {Object} product
 * @returns {Promise<Basket>}
 */
 const addItem = async (userId, product) => {
  const basket = await Basket.find({ userId: userId });

  if (!basket) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Basket not found');
  }

  await Basket.updateOne(
    { _id: basket[0].id },
    {
      $push: { products: product },
      $set: { total: basket[0].total + product.price }
    }
  );

  let updatedBasket =await getById(basket[0].id);
  return updatedBasket;
};

/**
 * Delete Item in basket
 * @param {ObjectId} userId
 * @param {Object} product
 * @returns {Promise<Basket>}
 */
 const deleteItem = async (userId, product) => {
   const basket = await Basket.find({userId: userId});
  if (!basket) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Basket not found');
  }

  await Basket.updateOne(
    { _id: basket[0].id },
    {
      $pop: { products: product },
      $set: { total: basket[0].total - product.price }
    }
  );

  let updatedBasket =await getById(basket[0].id);
  return updatedBasket;
};


module.exports = {
  create,
  getAll,
  getById,
  updateById,
  deleteById,
  addItem,
  deleteItem,
};
