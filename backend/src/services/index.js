module.exports.authService = require('./auth.service');
module.exports.tokenService = require('./token.service');
module.exports.userService = require('./user.service');
module.exports.categoryService = require('./category.service');
module.exports.regionService = require('./region.service');
module.exports.productService = require('./product.service');
module.exports.basketService = require('./basket.service');
