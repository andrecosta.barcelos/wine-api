const httpStatus = require('http-status');
const { Region } = require('../models');
const ApiError = require('../utils/ApiError');

/**
 * Create a region
 * @param {Object} regionBody
 * @returns {Promise<Region>}
 */
const create = async (regionBody) => {
  const region = await Region.create(regionBody);
  return region;
};

/**
 * Query for regions
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const getAll = async (filter, options) => {
  const regions = await Region.paginate(filter, options);
  return regions;
};

/**
 * Get region by id
 * @param {ObjectId} id
 * @returns {Promise<Region>}
 */
const getById = async (id) => {
  return Region.findById(id);
};

/**
 * Update Region by id
 * @param {ObjectId} regionId
 * @param {Object} updateBody
 * @returns {Promise<Region>}
 */
const updateById = async (regionId, updateBody) => {
  const region = await getById(regionId);
  if (!region) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Region not found');
  }
  Object.assign(region, updateBody);
  await region.save();
  return region;
};

/**
 * Delete region by id
 * @param {ObjectId} regionId
 * @returns {Promise<Region>}
 */
const deleteById = async (regionId) => {
  const region = await getById(regionId);
  if (!region) {
    throw new ApiError(httpStatus.NOT_FOUND, 'region not found');
  }
  await region.remove();
  return region;
};

module.exports = {
  create,
  getAll,
  getById,
  updateById,
  deleteById,
};
