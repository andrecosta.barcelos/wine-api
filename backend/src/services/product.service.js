const httpStatus = require('http-status');
const { Product } = require('../models');
const ApiError = require('../utils/ApiError');

/**
 * Create a product
 * @param {Object} productBody
 * @returns {Promise<Product>}
 */
const create = async (productBody) => {
  const product = await Product.create(productBody);
  return product;
};

const getFilters = async (filter) => {
  const finalFilter = {};

  // Name
  if (filter.name) {
    finalFilter.name = filter.name;
  }

  // Price
  if (filter.minprice && filter.maxprice) {
    finalFilter.price = { $gte: filter.minprice, $lte: filter.maxprice };
  } else if (filter.minprice) {
    finalFilter.price = { $gte: filter.minprice };
  } else if (filter.maxprice) {
    finalFilter.price = { $lte: filter.maxprice };
  }

  // Rating
  if (filter.minrating && filter.maxrating) {
    finalFilter.rating = { $gte: filter.minrating, $lte: filter.maxrating };
  } else if (filter.minrating) {
    finalFilter.rating = { $gte: filter.minrating };
  } else if (filter.maxrating) {
    finalFilter.rating = { $lte: filter.maxrating };
  }

  // Year
  if (filter.year) {
    finalFilter.year = filter.year;
  }

  // Category
  if (filter.category) {
    finalFilter['category.name'] = filter.category;
  }

  // Region
  if (filter.region) {
    finalFilter['region.name'] = filter.region;
  }

  return finalFilter;
};

/**
 * Query for products
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const getAll = async (filter, options) => {
  const finalFilter = await getFilters(filter);

  const products = await Product.paginate(finalFilter, options);

  return products;
};

/**
 * Get product by id
 * @param {ObjectId} id
 * @returns {Promise<Product>}
 */
const getById = async (id) => {
  return Product.findById(id);
};

/**
 * Update Product by id
 * @param {ObjectId} productId
 * @param {Object} updateBody
 * @returns {Promise<Product>}
 */
const updateById = async (productId, updateBody) => {
  const product = await getById(productId);
  if (!product) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Product not found');
  }
  Object.assign(product, updateBody);
  await product.save();
  return product;
};

/**
 * Delete product by id
 * @param {ObjectId} productId
 * @returns {Promise<Product>}
 */
const deleteById = async (productId) => {
  const product = await getById(productId);
  if (!product) {
    throw new ApiError(httpStatus.NOT_FOUND, 'product not found');
  }
  await product.remove();
  return Product;
};

module.exports = {
  create,
  getAll,
  getById,
  updateById,
  deleteById,
};
