const express = require('express');

// Core Routes
const authRoute = require('./auth.route');
const userRoute = require('./user.route');
const fileRoute = require('./file.route');

// Bacchus routes
const regionRoute = require('./region.route');
const categoryRoute = require('./category.route');
const productRoute = require('./product.route');
const backetRoute = require('./backet.route');


// Docs routes
const docsRoute = require('./docs.route');

// App configs
const config = require('../../config/config');


const router = express.Router();

// Default Routes available in all modes
const defaultRoutes = [
  {
    path: '/auth',
    route: authRoute,
  },
  {
    path: '/users',
    route: userRoute,
  },
  {
    path: '/regions',
    route: regionRoute,
  },
  {
    path: '/categories',
    route: categoryRoute,
  },
  {
    path: '/products',
    route: productRoute,
  },
  {
    path: '/files',
    route: fileRoute,
  },
  {
    path: '/backets',
    route: backetRoute,
  },
];

// Routes available only in development mode
const devRoutes = [
  {
    path: '/docs',
    route: docsRoute,
  },
];

/* Create routes to be used in api */
defaultRoutes.forEach((route) => {
  router.use(route.path, route.route);
});

/* Create routes to be used in development */
if (config.env === 'development') {
  devRoutes.forEach((route) => {
    router.use(route.path, route.route);
  });
}

module.exports = router;
