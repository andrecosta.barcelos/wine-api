var multer  = require('multer');

var storage = multer.diskStorage({
  // Destination when file is saved
  destination: '/uploads',
  filename: function (req, file, cb) {
    const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
    cb(null, 'image-' + uniqueSuffix + '.' + file.originalname.split('.').pop())
  }
})

module.exports = storage;
