const types = [
  // IMAGEs
  { mimetype: 'image/png', extension: ['png'], type: 'IMAGE' },
  { mimetype: 'image/bmp', extension: ['bmp'], type: 'IMAGE' },
  { mimetype: 'image/gif', extension: ['gif'], type: 'IMAGE' },
  { mimetype: 'image/x-icon', extension: ['ico'], type: 'IMAGE' },
  { mimetype: 'image/jpeg', extension: ['jpg', 'jpeg'], type: 'IMAGE' },
  { mimetype: 'image/svg+xml', extension: ['svg'], type: 'IMAGE' },
  { mimetype: 'image/webp', extension: ['webp'], type: 'IMAGE' },
  // DOCUMENTs
  { mimetype: 'application/msword', extension: ['doc'], type: 'DOCUMENT' },
  { mimetype: 'text/markdown', extension: ['md'], type: 'DOCUMENT' },
  { mimetype: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', extension: ['docx'], type: 'DOCUMENT' },
  { mimetype: 'application/vnd.oasis.opendocument.presentation', extension: ['odp'], type: 'DOCUMENT' },
  { mimetype: 'application/vnd.oasis.opendocument.spreadsheet', extension: ['ods'], type: 'DOCUMENT' },
  { mimetype: 'application/vnd.oasis.opendocument.text', extension: ['odt'], type: 'DOCUMENT' },
  { mimetype: 'application/pdf', extension: ['pdf'], type: 'DOCUMENT' },
  { mimetype: 'application/vnd.ms-powerpoint', extension: ['ppt'], type: 'DOCUMENT' },
  { mimetype: 'application/vnd.openxmlformats-officedocument.presentationml.presentation', extension: ['pptx'], type: 'DOCUMENT' },
  { mimetype: 'application/x-rar-compressed', extension: ['rar'], type: 'DOCUMENT' },
  { mimetype: 'application/x-tar', extension: ['tar'], type: 'DOCUMENT' },
  { mimetype: 'text/plain', extension: ['txt'], type: 'DOCUMENT' },
  { mimetype: 'application/vnd.ms-excel', extension: ['xls'], type: 'DOCUMENT' },
  { mimetype: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', extension: ['xlsx'], type: 'DOCUMENT' },
  { mimetype: 'application/xml', extension: ['xml'], type: 'DOCUMENT' },
  { mimetype: 'application/zip', extension: ['zip'], type: 'DOCUMENT' },
  { mimetype: 'application/x-7z-compressed', extension: ['7z'], type: 'DOCUMENT' },
  // VIDEOs
  { mimetype: 'video/avi', extension: ['avi'], type: 'VIDEO' },
  { mimetype: 'video/mpeg', extension: ['mpeg'], type: 'VIDEO' },
  { mimetype: 'video/ogg', extension: ['ogv'], type: 'VIDEO' },
  { mimetype: 'video/mp4', extension: ['mp4', 'm4v'], type: 'VIDEO' },
  { mimetype: 'video/quicktime', extension: ['mov'], type: 'VIDEO' },
  { mimetype: 'video/webm', extension: ['webm'], type: 'VIDEO' },
];

module.exports = types;
