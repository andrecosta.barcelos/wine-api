const roles = ['user', 'admin'];

const roleRights = new Map();
roleRights.set(roles[0], ['read']);
roleRights.set(roles[1], ['read', 'manage']);

module.exports = {
  roles,
  roleRights,
};
