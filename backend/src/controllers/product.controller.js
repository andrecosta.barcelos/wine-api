const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const Config = require('../config/config.js');
const catchAsync = require('../utils/catchAsync');
const socketStore = require('../utils/storeSocket');
const { productService } = require('../services');

const getImageUrl = (image) => {
  return Config.mediaurl + image;
};

const createProduct = catchAsync(async (req, res) => {

  socketStore.sendMessage('newProduct', {
    title: 'Novo Vinho!',
    message: req.body.name,
  });

  const product = await productService.create(req.body);
  res.status(httpStatus.CREATED).send(product);
});

const getProducts = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['name', 'minprice', 'maxprice', 'minrating', 'maxrating', 'year', 'category', 'region']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);

  const result = await productService.getAll(filter, options);

  result.results.forEach(async (p) => {
    const url = getImageUrl(p.image.file);
    p.image.file = url;
  });

  res.send(result);
});

const getProduct = catchAsync(async (req, res) => {
  const product = await productService.getById(req.params.id);
  if (!product) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Product not found');
  }

  const url = getImageUrl(product.image.file);
  product.image.file = url;
  res.send(product);
});

const updateProduct = catchAsync(async (req, res) => {
  const product = await productService.updateById(req.params.id, req.body);
  res.send(product);
});

const deleteProduct = catchAsync(async (req, res) => {
  await productService.deleteById(req.params.id);
  res.status(httpStatus.NO_CONTENT).send();
});

module.exports = {
  createProduct,
  getProducts,
  getProduct,
  updateProduct,
  deleteProduct,
};
