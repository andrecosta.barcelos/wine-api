const httpStatus = require('http-status');
const multer  = require('multer');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const fileService = require('../services/file.service');
const  diskStorage = require('../config/diskStorage');

// Multer
const uploadFile = multer({
  storage: diskStorage,
}).single('file');

const createFile = catchAsync(async (req, res) => {

  uploadFile(req, res, async (error) => {

    if (!error) {

      let fileData = {
        file: req.file.filename,
        path: req.file.path,
        mimetype: req.file.mimetype,
        description: req.body.description,
      }

      const file = await fileService.create(fileData);
      res.status(httpStatus.CREATED).send(file);

    } else  {
      res.send(error);
    }

  });

});

const getFiles = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['mimetype']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const result = await fileService.getAll(filter, options);
  res.send(result);
});

const getFile = catchAsync(async (req, res) => {
  const file = await fileService.getById(req.params.id);
  if (!file) {
    throw new ApiError(httpStatus.NOT_FOUND, 'File not found');
  }
  res.send(file);
});

const updateFile = catchAsync(async (req, res) => {

  uploadFile(req, res, async (error) => {

    if (!error) {

      let fileData = {
        file: req.file.filename,
        path: req.file.path,
        mimetype: req.file.mimetype,
        description: req.body.description,
      }

      const file = await fileService.updateById(req.params.id, fileData);
      res.send(file);

    } else  {
      res.send(error);
    }

  });

});

const deleteFile = catchAsync(async (req, res) => {
  await fileService.deleteById(req.params.id);
  res.status(httpStatus.NO_CONTENT).send();
});

module.exports = {
  createFile,
  getFiles,
  getFile,
  updateFile,
  deleteFile,
};
