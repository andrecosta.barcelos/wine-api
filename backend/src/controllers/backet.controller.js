const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { basketService, productService } = require('../services');


const getBackets = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const result = await basketService.getAll(filter, options);
  res.send(result);
});

const getBacket = catchAsync(async (req, res) => {
  const basket = await basketService.getById(req.params.id);
  if (!basket) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Basket not found');
  }
  res.send(basket);
});

const addProduct = catchAsync(async (req, res) => {

  const product = await productService.getById(req.body.productId);

  const basket = await basketService.addItem(req.user.id, product);
  res.send(basket);
});

const deleteProduct = catchAsync(async (req, res) => {

  const product = await productService.getById(req.body.productId);

  const basket = await basketService.deleteItem(req.user.id, product);
  res.send(basket);
});

module.exports = {
  getBackets,
  getBacket,
  addProduct,
  deleteProduct,
};
