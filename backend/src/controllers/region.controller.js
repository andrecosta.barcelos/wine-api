const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { regionService } = require('../services');

const createRegion = catchAsync(async (req, res) => {
  const region = await regionService.create(req.body);
  res.status(httpStatus.CREATED).send(region);
});

const getRegions = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['name', 'location']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const result = await regionService.getAll(filter, options);
  res.send(result);
});

const getRegion = catchAsync(async (req, res) => {
  const region = await regionService.getById(req.params.id);
  if (!region) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Region not found');
  }
  res.send(region);
});

const updateRegion = catchAsync(async (req, res) => {
  const region = await regionService.updateById(req.params.id, req.body);
  res.send(region);
});

const deleteRegion = catchAsync(async (req, res) => {
  await regionService.deleteById(req.params.id);
  res.status(httpStatus.NO_CONTENT).send();
});

module.exports = {
  createRegion,
  getRegions,
  getRegion,
  updateRegion,
  deleteRegion,
};
