module.exports.authController = require('./auth.controller');
module.exports.userController = require('./user.controller');
module.exports.regionController = require('./region.controller');
module.exports.categoryController = require('./category.controller');
module.exports.productController = require('./product.controller');
module.exports.fileController = require('./file.controller');
module.exports.backetController = require('./backet.controller');
