const cache = require('../utils/cache');

const cacheService = () => async (req, res, next) => {
  // Save cache instance
  req.cache = cache;
  return next();
};

module.exports = { cacheService };
