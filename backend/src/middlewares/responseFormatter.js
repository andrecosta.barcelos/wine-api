
const methods = require('./../config/responseMethods');


/**
 * Generate success response
 * @param {*} data
 * @param {*} total
 * @returns
 */
const successResponse = (data = [], link = {}) => {

  // Check is data array
  if (!(data instanceof Array)) {
    data = [data];
  }

  return {
    status: 'success',
    link: link,
    data: data,
    errors: [],
  };
};


/**
 * Generate error response
 * @param {*} errors
 * @returns
 */
const errorResponse = (errors = []) => {

  if (!(errors instanceof Array)) {
    errors = [errors];
  }

  return {
    status: 'error',
    link: {},
    data: [],
    errors: errors,
  };
};


// Set Response methods
const generateFormatters = (res) => {
  const formatter = {};
  let responseBody = {};

  methods.forEach(method => {
    if (method.isSuccess) {

     // Success Response
      if (parseInt(method.code, 10) !== 204) {
        formatter[method.name] = (data, link) => {
          responseBody = successResponse(data, link );
          res.status(method.code).json(responseBody);
        };
      }
      // No content exception
      else
      {
        formatter[method.name] = () => {
          res.status(204).send();
        };
      }
    } else {
      formatter[method.name] = (errors = []) => {
        responseBody = errorResponse(errors);
        res.status(method.code).json(responseBody);
      };
    }
  });

  return formatter;
};

const responseFormatters = () => (req, res, next) => {
  res.formatter = generateFormatters(res);
  next();
};


module.exports = responseFormatters;
