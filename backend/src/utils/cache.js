const Redis = require('ioredis');
const logger = require('../config/logger');
const config = require('../config/config');

class Cache {
  constructor() {
    this.redis = new Redis({
      host: config.redis.host,
      port: config.redis.port,
      password: config.redis.password,
      keyPrefix: 'cache:',
    });
    logger.info('Connected to Redis');
  }

  // Get Value in cache by key
  async get(key) {
    const value = await this.redis.get(key);

    return value ? JSON.parse(value) : null;
  }

  // Save value in cache
  set(key, value) {
    return this.redis.set(key, JSON.stringify(value), 'EX', parseInt(config.redis.keyTimeToExpired, 10));
  }

  // Delete key in cache
  delete(key) {
    return this.redis.del(key);
  }

  async deletePrefix(prefix) {
    const keys = (await this.redis.keys(`cache:${prefix}:*`)).map((key) => key.replace('cache:', ''));

    return this.redis.del(keys);
  }
}

module.exports = new Cache();
