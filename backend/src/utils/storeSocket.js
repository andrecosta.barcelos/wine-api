var cache = require('memory-cache');

// default options for notifications
let defaultOptions = {
  theme: 'light',
  duration: 3000,
  backgroundTop: 'green',
  backgroundBottom: 'darkgreen',
  colorTop: 'green',
  colorBottom: 'darkgreen',
  closeButton: 'Fechar',
  native: false,
  vibrate: 0,
  silent: true
}

// Get socket
const getSocket = () => {
  cache.get('socket');
}

// Set socket
const setSocket = (socket) => {
  cache.put('socket', socket);
}

// Delete socket
const deleteSocket = (socket) => {
  cache.del('socket');
}

// message
/*
   title: 'title',
   subtitle: 'subtitle', //optional
   message: 'message', //optional
   icon?: string
*/

const sendMessage = (topic, message, options = {}) => {

  let socket = cache.get('socket');

  if (Object.keys(options).length !== 0) {
    defaultOptions = {... defaultOptions, ...options };
  }

  if (socket !== null) {
    console.log(socket);
     console.log("Envio de notificação");
     socket.emit(topic, {message: message, options: defaultOptions} );
  }
}

module.exports = {
  getSocket,
  setSocket,
  deleteSocket,
  sendMessage,
};
